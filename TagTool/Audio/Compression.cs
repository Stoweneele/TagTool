﻿namespace TagTool.Audio
{
    public enum Compression : sbyte
    {
        PCM = 0x3,  //RAW PCM
        MP3 = 0x8,  //MP3
        FSB4 = 0x9  //FMOD System Bank Type 4
    }
}